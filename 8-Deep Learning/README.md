# Trabalho Deep Learning - Avaliação do Índice da Ibovespa
Neste trabalho iremos implementar uma RNNs para realizar a predição diária  de um ativo da bolsa de valores, como exemplo temos a análise do Mini índice da Bovespa. Você deverá escolher o ativo de sua preferência no site: https://br.financas.yahoo.com/ e seguir os passos do exemplo.

O arquivo do Trabalho pode ser encontrado em https://github.com/adrianosanick/InteligenciaArtificial na pasta:

```
1-RNAs
Solucoes
TrabalhoLSTM.ipynb
```

O que deve ser entregue: Um arquivo *.pdf contendo um resumo da análise do ativo previsto e o seu gráfico de predição.
