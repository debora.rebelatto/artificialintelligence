# Artificial Intelligence

Subject repository

```
artificialintelligence/
┣ 📂 1 - Tarefa 1/
┃ ┗ 📜 prolog.txt
┣ 📂 2 - Tarefa Extra/
┃ ┣ 📜 11-Introducao.ipynb
┃ ┗ 📜 12-Exercicios.ipynb
┣ 📂 3 - RNA Simples/
┃ ┣ 📜 Tarefa1_-_RNA_simples
┃ ┣ 📜 Tarefa2-GradianteDecrescente
┃ ┗ 📜 Tarefa3-AtualizaPesos
┣ 📂 4 - RNA MPL/
┃ ┗ 📜 Tarefa4_-_RNA_MPL_4X3x2
┣ 📂 5 - RNA BackPropagation/
┃ ┗ 📜 Tarefa5_-_RNA_MPL_backward.ipynb
┣ 📂 6-Algoritmo Backpropagation/
┃ ┗ 📜 Tarefa6-RNA_MPL_Backpropagation.ipynb
┣ 📂 7-Trabalho RNA/
┃ ┗ 📜 TrabalhoRNA.ipynb
┣ 📂 8-Trabalho RNA/
┃ ┗ 📜 TrabalhoLSTM.ipynb
┣ 📂 pdfTarefas
┗ README.md
```