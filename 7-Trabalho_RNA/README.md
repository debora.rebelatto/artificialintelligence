# 7 - Trabalho RNA

Este trabalho é a finalização do processo de aprendizado sobre RNAs.
Ele finaliza a caminhada através das tarefas apresentadas. Nele você terá
que modificar o que for preciso para que a sua rede aprenda com o dataset
'arruela_.csv' e tenha uma acurácia superior a apresentada pelo professor (0.86).
Analise as entradas e suas relações com a saída, verifique a evolução do treinamento
e sua relação com a taxa de aprendizagem.

O que você deve entregar (em um arquivo *.pdf):

- Nomes dos elementos do grupo (máx. 3)
- Endereço do repositório do git para o professor avaliar o código
- Apreciação sobre o trabalho e os resultados:
  - De 150 a 500 palavras e deve ser seguido das palavras-chave representativas do conteúdo do trabalho (ente três e seis palavras-chave)
OBS: Embora o trabalho possa ser realizado em grupo, a entrega deve ser individual e o endereço do repositório deve ser pessoal.

O arquivo do Trabalho pode ser encontrado em https://github.com/adrianosanick/InteligenciaArtificial na pasta:

```
1 - RNAs
└─── Solucoes
  │   TrabalhoRNA.ipynb
  │   file012.txt
  │   dataset 'arruela_.csv'
```