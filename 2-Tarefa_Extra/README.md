# Tarefa Extra (1 ponto extra na NE)
Está é uma tarefa extra para aqueles acadêmicos que desejam praticar alguns exercícios referentes as bibliotecas que serão utilizadas durante a disciplina:

12-Exercicios.ipynb  
22-NumpyExercises.ipynb  
32-MatplotlibExercises.ipynb  
Estes exercícios poderão ser clonados do repositório da disciplina: https://github.com/adrianosanick/InteligenciaArtificial na pasta ExercicioPython.

