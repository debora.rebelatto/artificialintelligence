# Tarefas RNAs - Backpropagation

Esta tarefa é uma sequência das anteriores e também deverá ser postada em um arquivo *.pdf. O arquivo da tarefa pode ser encontrado em https://github.com/adrianosanick/InteligenciaArtificial na pasta 1-RNAs.

- Tarefa 5 - Backpropagation: Abaixo, você implementará o código para calcular uma rodada de atualização com backpropagation para dois conjuntos de pesos. Escrevi o andamento para frente, o seu objetivo é escrever o andamento para trás. Coisas a fazer:
  - Calcular o erro da rede.
  - Calcular o gradiente de erro da camada de output.
  - Usar a backpropagation para calcular o erro da camada oculta.
  - Calcular o passo de atualização dos pesos.