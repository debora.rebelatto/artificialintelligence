# Tarefas RNAs - RNA Simples (1 ponto na NE)
Esta tarefa reúne um conjunto de tarefas simples que deverão ser postadas juntas em um arquivo *.pdf. Estas tarefas poderão ser encontradas em https://github.com/adrianosanick/InteligenciaArtificial na pasta 1-RNAs.

- Tarefa 1 - Rede Neural Simples (RNA.py)
- Tarefa 2 - Gradiente Decrescente (gradiente.py)
- Tarefa 3 - Exercício de programação : O objetivo é treinar a rede até que ela alcance um mínimo na média de erros quadrados (MEQ) dos dados de treinamento. Você deverá implementar:
  - O output da rede: output.
  - O erro do output: error.
  - O termo do erro: error_term.
  - Atualizar o passo: del_w +=.
  - Atualizar os pesos: weights +=.
OBS: Para realizar a Tarefa 3, utilize os códigos disponíveis na tarefa 2. O arquivo a ser alterado é o gradient.py.
