# Tarefas RNAs - RNA MPL
Esta tarefa é uma sequência das anteriores e também deverá ser postada em um arquivo *.pdf. O arquivo da tarefa pode ser encontrado em https://github.com/adrianosanick/InteligenciaArtificial na pasta 1-RNAs.

- Tarefa 4 - MPL 4x3x2:  A seguir, você implementará uma rede 4x3x2 orientada a frente, com funções de ativação sigmóide em ambas as camadas. Coisas a fazer:
  - Calcular o input da camada oculta.
  - Calcular o output da camada oculta.
  - Calcular o input da camada de output.
  - Calcular o output da rede.